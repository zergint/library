-- phpMyAdmin SQL Dump
-- version 2.11.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 17 2019 г., 23:23
-- Версия сервера: 5.0.51
-- Версия PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- База данных: `demo`
--

CREATE DATABASE `demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `demo`;
-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `book_condition` varchar(255) NOT NULL default ''NEW'',
  `status` varchar(255) NOT NULL default ''Доступна'',
  `reader_id` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `reader_id` (`reader_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`, `book_condition`, `status`, `reader_id`) VALUES
(34, ''Война и мир'', ''новая'', ''Выдана'', 53),
(28, ''Жизнь людей'', ''хорошее'', ''Выдана'', 53),
(35, ''PHP'', ''новая'', ''Доступна'', NULL),
(30, ''С# для чайников'', ''хорошее'', ''Выдана'', 47),
(31, ''Selenium'', ''хорошее'', ''Выдана'', 50),
(32, ''Java для чайников'', '''', ''Выдана'', 54),
(33, ''Философия Java'', ''новая'', ''Выдана'', 50),
(37, ''Тестирование'', ''новая'', ''Доступна'', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `readers`
--

CREATE TABLE IF NOT EXISTS `readers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Дамп данных таблицы `readers`
--

INSERT INTO `readers` (`id`, `name`) VALUES
(47, ''Света''),
(50, ''Миша''),
(54, ''Толя''),
(53, ''Костя''),
(48, ''Вася'');
