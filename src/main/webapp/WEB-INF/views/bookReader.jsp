<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="${contextPath}/resources/favicon.png" type="image/png">
    <title>Список читателей</title>
</head>
<body>
<div class="container">
    <br>
    <form method="get">
        <a class="btn btn-primary" href="<c:url value="/"/>" role="button">На главную</a>
        <a class="btn btn-primary" href="<c:url value="/books"/>" role="button">Список книг</a>

    </form>
    <h3>
        Добавить читателя
    </h3>

    <c:url var="addAction" value="/bookReader/add"/>

    <form:form action="${addAction}" modelAttribute="bookReader">
        <table>
            <c:if test="${!empty bookReader.name}">
                <tr>
                    <td>
                        <form:label path="id">
                            <spring:message text="ID"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="id" readonly="true" size="8" disabled="true"/>
                        <form:hidden path="id"/>
                    </td>
                </tr>
            </c:if>
            <tr>
                <td>
                    <form:label path="name">
                        <spring:message text="Имя"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="name"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <c:if test="${!empty bookReader.name}">
                        <button type="submit" class="btn btn-success">Изменить</button>
                    </c:if>
                    <c:if test="${empty bookReader.name}">
                        <button type="submit" class="btn btn-success">Добавить</button>
                    </c:if>
                </td>
            </tr>
        </table>
    </form:form>
    <br>
    <h3>Список читателей</h3>

    <c:if test="${!empty listBookReaders}">
        <table class="table table-bordered table-dark table-sm">
            <tr>
                <th scope="col" style="width: 5%">ID</th>
                <th scope="col" style="width: 20%">Имя</th>
                <th></th>
                <th scope="col" style="width: 10%">Список книг</th>
                <th scope="col" style="width: 5%">Изменить</th>
                <th scope="col" style="width: 5%">Удалить</th>
            </tr>
            <c:forEach items="${listBookReaders}" var="bookReader">
                <tr>
                    <td>${bookReader.id}</td>
                    <td>${bookReader.name}</td>
                    <td></td>
                    <td><a class="btn btn-success" href="<c:url value='/readerBooks/${bookReader.id}' />" role="button">Книги</a>
                    </td>
                    <td><a class="btn btn-success" href="<c:url value='/editBookReader/${bookReader.id}' />"
                           role="button">Изменить</a>
                    </td>

                    <td>
                        <c:if test="${empty bookReader.books}">


                            <a class="btn btn-success" href="<c:url value='/removeBookReader/${bookReader.id}' />"
                               role="button">Удалить</a>
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

    <c:if test="${!empty listReaderBooks}">
        <h3>Список книг</h3>
        <table class="table table-bordered table-dark table-sm">
            <thead>
            <tr>
                <th scope="col" style="width: 4%">ID</th>
                <th scope="col" style="width: 20%">Название</th>
                <th scope="col">Состояние</th>
            </tr>
            </thead>
            <c:forEach items="${listReaderBooks}" var="book">
                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.condition}</td>
                </tr>
            </c:forEach>
        </table>

    </c:if>
</div>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
