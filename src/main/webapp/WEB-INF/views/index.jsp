<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="${contextPath}/resources/favicon.png" type="image/png">

    <title>Библиотека</title>
</head>
<body>
<div class="container">
    <h1>Библиотека</h1>
    <a class="btn btn-primary btn-lg" href="<c:url value="/books"/>" role="button">Список книг</a>
    <a class="btn btn-primary btn-lg" href="<c:url value="/bookReaders"/>" role="button">Список читателей</a>

</div>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>