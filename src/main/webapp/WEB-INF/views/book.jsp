<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link rel="shortcut icon" href="${contextPath}/resources/favicon.png" type="image/png">

    <title>Список книг</title>
</head>
<body>
<div class="container">
    <br>
    <form method="get">
        <a class="btn btn-primary" href="<c:url value="/"/>" role="button">На главную</a>
        <a class="btn btn-primary" href="<c:url value="/bookReaders"/>" role="button">Список читателей</a>

    </form>
    <h3>
        Добавить книгу
    </h3>

    <c:url var="addAction" value="/book/add"/>

    <form:form action="${addAction}" modelAttribute="book">
        <table>
            <tr>
                <td>
                    <form:label path="name">
                        <spring:message text="Название"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="name"/>
                </td>
            </tr>
            <tr>
                <td>
                    <form:label path="condition">
                        <spring:message text="Состояние"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="condition"/>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-success">Добавить
                    </button>


                </td>
            </tr>
        </table>
    </form:form>

    <c:url var="addActionSearch" value="/search"/>

    <form:form action="${addActionSearch}" modelAttribute="filter">
        <table>

                <tr>
                    <td>
                        <form:label path="search">
                            <spring:message text="Поиск"/>
                        </form:label>
                    </td>
                    <td>
                        <form:input path="search" />

                    </td>
                </tr>

            <tr>
                <td colspan="2">

                        <button type="submit" class="btn btn-success">Найти</button>

                </td>
            </tr>
        </table>
    </form:form>


    <h3>Список книг</h3>
    <c:if test="${!empty listBooks}">

        <table class="table table-bordered table-dark table-sm">
            <thead>
            <tr>
                <th scope="col" style="width: 4%">ID</th>
                <th scope="col" style="width: 15%">Название</th>
                <th scope="col">Состояние</th>

                <th scope="col" style="width: 5%">Доступность</th>
                <th scope="col" style="width: 10%">Читатель</th>

                <th scope="col" style="width: 30%">

                </th>
                <th scope="col" style="width: 5%">Удалить</th>
            </tr>
            </thead>
            <c:forEach items="${listBooks}" var="book">
                <tr>
                    <td>${book.id}</td>
                    <td>${book.name}</td>
                    <td>${book.condition}</td>
                    <td>${book.status}</td>
                    <td>${book.bookReader.name}</td>
                    <td>
                        <ul style="padding:0; margin-left:5px;">
                            <li style="float: left;">
                                <c:if test="${book.status eq 'Выдана'}">
                                    <c:url var="returnBook" value="/returnBook/${book.id}"/>
                                    <form:form method="post" action="${returnBook}" modelAttribute="book"
                                               id="returnBook${book.id}" style="margin: 0">


                                        <form:input type="text" path="condition"
                                                    placeholder="опишите состояние"/>
                                    </form:form>
                                </c:if>

                                <c:if test="${book.status ne 'Выдана'}">
                                    <c:url var="giveBook" value="/giveBook/${book.id}"/>
                                    <form:form method="post" modelAttribute="bookReader" action="${giveBook}"
                                               id="giveBook${book.id}" cssStyle="margin: 0">

                                        <form:select path="id">
                                            <form:option value="0" label="---- Выбор читателя ----"/>
                                            <form:options items="${listBookReaders}" itemValue="id" itemLabel="name"/>
                                        </form:select>

                                    </form:form>


                                </c:if>
                            </li>
                            <li style="float: right">

                                <c:if test="${book.status ne 'Выдана'}">

                                    <button form="giveBook${book.id}" type="submit" class="btn btn-success">Выдать
                                    </button>

                                </c:if>

                                <c:if test="${book.status eq 'Выдана'}">
                                    <button form="returnBook${book.id}" type="submit" class="btn btn-success">Вернуть
                                    </button>


                                </c:if>
                            </li>
                        </ul>
                    </td>
                    <td>

                        <c:if test="${book.bookReader eq null}">
                            <a class="btn btn-success" href="<c:url value='/removeBook/${book.id}' />"
                               role="button">Удалить</a>
                        </c:if>

                    </td>
                </tr>
            </c:forEach>
        </table>

    </c:if>
</div>

<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>
