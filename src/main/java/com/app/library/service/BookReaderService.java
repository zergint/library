package com.app.library.service;


import com.app.library.model.BookReader;

import java.util.List;

public interface BookReaderService {
    List<BookReader> listBookReaders();

    void addBookReader(BookReader b);

    BookReader getBookReaderById(Long id);

    void removeBookReader(Long id);
}
