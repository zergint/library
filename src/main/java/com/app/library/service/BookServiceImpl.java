package com.app.library.service;

import com.app.library.model.Book;
import com.app.library.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Book> listBooks() {
        return this.bookRepository.findAllByOrderByIdAsc();
    }

    @Override
    @Transactional
    public List<Book> searchListBooks(String filter) {

        return this.bookRepository.findAllByNameLikeOrderByIdAsc(filter);
    }

    @Override
    @Transactional
    public void addBook(Book b) {
        this.bookRepository.save(b);
    }

    @Override
    @Transactional
    public Book getBookById(Long id) {
        return this.bookRepository.findOne(id);
    }

    @Override
    @Transactional
    public void removeBook(Long id) {
        this.bookRepository.delete(id);
    }

}
