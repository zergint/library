package com.app.library.service;

import com.app.library.model.BookReader;
import com.app.library.repository.BookReaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookReaderServiceImpl implements BookReaderService {
    @Autowired
    private BookReaderRepository bookReaderRepository;

    @Override
    public List<BookReader> listBookReaders() {
        return bookReaderRepository.findAllByOrderByIdAsc();
    }

    @Override
    @Transactional
    public void addBookReader(BookReader b) {
        this.bookReaderRepository.save(b);
    }

    @Override
    @Transactional
    public BookReader getBookReaderById(Long id) {
        return this.bookReaderRepository.findOne(id);
    }

    @Override
    @Transactional
    public void removeBookReader(Long id) {
        this.bookReaderRepository.delete(id);
    }

}
