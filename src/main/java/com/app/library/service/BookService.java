package com.app.library.service;

import com.app.library.model.Book;

import java.util.List;

public interface BookService {
    List<Book> listBooks();

    void addBook(Book b);

    List<Book> searchListBooks(String filter);

    Book getBookById(Long id);

    void removeBook(Long id);
}
