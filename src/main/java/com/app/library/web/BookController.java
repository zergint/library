package com.app.library.web;

import com.app.library.model.Book;
import com.app.library.model.BookReader;
import com.app.library.model.Filter;
import com.app.library.service.BookReaderService;
import com.app.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@SessionAttributes(value = "filter")
public class BookController {
    @Autowired
    private BookService bookService;

    @Autowired
    private BookReaderService bookReaderService;

    //главная для книг
    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String listBooks(@ModelAttribute("filter") Filter f, Model model) {
        model.addAttribute("book", new Book());

        if (f.getSearch().isEmpty()) {
            model.addAttribute("listBooks", this.bookService.listBooks());
        } else {
            model.addAttribute("filter", f);
            model.addAttribute("listBooks", this.bookService.searchListBooks("%" + f.getSearch() + "%"));
        }
        model.addAttribute("bookReader", new BookReader());
        model.addAttribute("listBookReaders", this.bookReaderService.listBookReaders());

        return "book";
    }

    @ModelAttribute("filter")
    public Filter populateForm() {
        Filter f = new Filter();
        f.setSearch("");

        return f;
    }

    //добавление книги
    @RequestMapping(value = "/book/add", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("book") Book b) {

        if (b.getId() == null) {
            b.setStatus("Доступна");
            this.bookService.addBook(b);
        }

        return "redirect:/books";

    }

    //удаление книги
    @RequestMapping("/removeBook/{id}")
    public String removeBook(@PathVariable("id") Long id, Model model) {
        this.bookService.removeBook(id);

        return "redirect:/books";
    }

    //выдача книги
    @RequestMapping(value = "/giveBook/{bookId}", method = RequestMethod.POST)
    public String give2Book(@PathVariable("bookId") Long bookId, @ModelAttribute("bookReader") BookReader r, Model model) {
        if (r.getId() != 0) {
            BookReader reader = this.bookReaderService.getBookReaderById(r.getId());
            Book b = this.bookService.getBookById(bookId);
            b.setStatus("Выдана");
            b.setBookReader(reader);
            this.bookService.addBook(b);
        }
        return "redirect:/books";
    }

    //возврат книги
    @RequestMapping(value = "/returnBook/{id}", method = RequestMethod.POST)
    public String returnBook(@PathVariable("id") Long id, @ModelAttribute("book") Book book, Model model) {
        Book b = this.bookService.getBookById(id);
        b.setStatus("Доступна");
        b.setCondition(book.getCondition());
        b.setBookReader(null);
        this.bookService.addBook(b);

        return "redirect:/books";
    }

    //поиск
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchBooks(@ModelAttribute("filter") Filter f, Model model) {
        model.addAttribute("filter", f);

        return "redirect:/books";
    }


}
