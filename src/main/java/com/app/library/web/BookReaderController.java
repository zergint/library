package com.app.library.web;

import com.app.library.model.Book;
import com.app.library.model.BookReader;
import com.app.library.service.BookReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

@Controller
public class BookReaderController {
    @Autowired
    private BookReaderService bookReaderService;

    @RequestMapping(value = "/bookReaders", method = RequestMethod.GET)
    public String listBooks(Model model) {
        model.addAttribute("bookReader", new BookReader());
        model.addAttribute("listBookReaders", this.bookReaderService.listBookReaders());

        return "bookReader";
    }

    //добавление-изменение читателя
    @RequestMapping(value = "/bookReader/add", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("bookReader") BookReader b) {

        this.bookReaderService.addBookReader(b);

        return "redirect:/bookReaders";
    }

    //удаления читателя
    @RequestMapping("/removeBookReader/{id}")
    public String removeBookReader(@PathVariable("id") Long id, Model model) {
        Set<Book> listBooks = this.bookReaderService.getBookReaderById(id).getBooks();

        if (listBooks.isEmpty()) {
            this.bookReaderService.removeBookReader(id);
        }

        model.addAttribute("bookReader", new BookReader());
        model.addAttribute("listBookReaders", this.bookReaderService.listBookReaders());

        return "bookReader";
    }

    //список книг читателя
    @RequestMapping("/readerBooks/{id}")
    public String listReaderBooks(@PathVariable("id") Long id, Model model) {
        model.addAttribute("listReaderBooks", this.bookReaderService.getBookReaderById(id).getBooks());
        model.addAttribute("bookReader", new BookReader());
        model.addAttribute("listBookReaders", this.bookReaderService.listBookReaders());

        return "bookReader";
    }

    //изменение читателя
    @RequestMapping("/editBookReader/{id}")
    public String editBookReader(@PathVariable("id") Long id, Model model) {
        model.addAttribute("bookReader", this.bookReaderService.getBookReaderById(id));
        model.addAttribute("listBookReaders", this.bookReaderService.listBookReaders());
        return "bookReader";
    }

}
