package com.app.library.repository;

import com.app.library.model.BookReader;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookReaderRepository extends JpaRepository<BookReader, Long> {
    List<BookReader> findAllByOrderByIdAsc();
}
